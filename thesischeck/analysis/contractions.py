"""Analysis of contractions.

Looks for uses of non-contracted words like 'is not' instead of isn't.
See https://en.wikipedia.org/wiki/Contraction_(grammar)
"""
import re

from analysis.annotation import Annotation
from analysis.utils import verify_match, matches_whole_words
from analysis.languages import Language


CONTRACTIONS_EN = {
    r'(has|have|is|was) not': "\\1n't",
    r'(I|we|they) have': "\\1've",
    r'(I|you|s?he|it|we|they) would': "\\1'd",
    r'(I|you|s?he|it|we|they) will': "\\1'll",
    "will not": "won't",
    r'(you|we|they) are': "\\1're",
    r'(t?here|it|s?he) is': "\\1's"
}

CONTRACTIONS_DE = {
    r'zu (dem|einem)': "zum",
    r'zu (der|einer)': "zur",
    r'in dem': "im",
    r'in das': "ins"
}


class Contraction(Annotation):
    def __init__(self, *args, word=None, contraction=None):
        annotation = f"Contractions are shorter and nice to read."
        args = args + (annotation,)
        super().__init__(*args)
        self.word = word
        self.contraction = contraction

    def __str__(self):
        lines = [
            f"line {self.linenumber} at character {self.index}: {self.annotation}",
            f"    Try to use {self.contraction} instead of {self.word}",
            self.annotation_indicator()
        ]
        return "\n".join(lines)


def analyze(line, linenum, lang):
    annotations = []

    if lang is Language.en_EN or lang is None:
        contractions = CONTRACTIONS_EN
    elif lang is Language.de_DE:
        contractions = CONTRACTIONS_DE

    for pattern in contractions:
        for match in re.finditer(pattern, line, flags=re.IGNORECASE):
            if (not verify_match(match, line)) or (not matches_whole_words(match, line)):
                continue

            index = match.start()
            replaced_contraction = re.sub(pattern, contractions[pattern], match.group(0), flags=re.IGNORECASE)
            annotation = Contraction(linenum, line, index, word=match.group(0), contraction=replaced_contraction)
            annotations.append(annotation)

    return annotations
