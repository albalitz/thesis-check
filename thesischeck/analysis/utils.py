import re

ignore_latex_commands = [
    "begin", "end",
    "renewcommand",
    "includegraphics",
    "url", "href",
    "label", "ref",
    "cite",
    "todo"
]

caps = "([A-Z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|de|eu|net|org|io|gov)"
digits = "([0-9])"

COMMENT_CHARS = ["%"]


def matches_whole_words(match, line):
    """Checks if a match matches whole words and not just the end and the beginning.

    Arguments:
        match -- A match object returned by most re functions
        line {str} -- the line in which the match was found

    Returns:
        bool -- True, if the match meets the criteria, False if it doesn't
    """
    before_match = line[:match.start()]
    after_match = line[match.end():]
    return before_match.endswith(" ") or after_match.startswith(" ")


def verify_match(match, line):
    """Check if a match meets specific criteria.

    Arguments:
        match -- A match object returned by most re functions
        line {str} -- the line in which the match was found

    Returns:
        bool -- True, if the match meets the criteria, False if it doesn't
    """
    # make sure the match isn't in a comment
    if any(c in line[:match.start()] for c in COMMENT_CHARS):
        return False

    return True


def cleanup_word(word):
    """Clean a word from specific syntax like LaTeX.

    Arguments:
        word {str} -- The word.

    Returns:
        str -- The cleaned word.
    """
    try:
        word = re.search(r'(\w+)\\footnote\{', word).group(1)
    except AttributeError:
        pass

    try:
        latex_command = re.search(r'\\(.+?)(\[.+?\])?\{', word).group(1)
        assert latex_command not in ignore_latex_commands
        word = re.search(r'\\.+?\{(.+)\}', word).group(1)
    except AttributeError:
        pass
    except AssertionError:
        return ""

    return word


def sentences(text):
    """Split the text into sentences.

    This makes sure not to split specific abbreviations
    by replacing specific punctuation marks before and restoring them after splitting.

    Based on https://stackoverflow.com/a/31505798 with minimal changes.

    Arguments:
        text {str} -- The text as one string.

    Returns:
        {list} -- A list of sentences from the text.
    """
    text = " " + text + "  "
    text = text.replace("\n", " ")
    text = re.sub(prefixes, "\\1<prd>", text)
    text = re.sub(websites, "<prd>\\1", text)

    text = text.replace("Ph.D.", "Ph<prd>D<prd>")
    text = text.replace("i.e.", "i<prd>e<prd>")
    text = text.replace("e.g.", "e<prd>g<prd>")

    text = re.sub("\s" + caps + "[.] ", " \\1<prd> ", text)
    text = re.sub(acronyms + " " + starters, "\\1<stop> \\2", text)
    text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]", "\\1<prd>\\2<prd>\\3<prd>", text)
    text = re.sub(caps + "[.]" + caps + "[.]", "\\1<prd>\\2<prd>", text)
    text = re.sub(" " + suffixes + "[.] " + starters, " \\1<stop> \\2", text)
    text = re.sub(" " + suffixes + "[.]", " \\1<prd>", text)
    text = re.sub(" " + caps + "[.]", " \\1<prd>", text)
    text = re.sub(digits + "[.]" + digits, "\\1<prd>\\2", text)

    if "”" in text:
        text = text.replace(".”", "”.")
    if "\"" in text:
        text = text.replace(".\"", "\".")
    if "!" in text:
        text = text.replace("!\"", "\"!")
    if "?" in text:
        text = text.replace("?\"", "\"?")

    text = text.replace(".", ".<stop>")
    text = text.replace("?", "?<stop>")
    text = text.replace("!", "!<stop>")
    text = text.replace("<prd>", ".")

    sentences = text.split("<stop>")
    sentences = [s.strip() for s in sentences[:-1]]

    return sentences
