"""Analysis of todos.

Looks for leftover or overlooked todo comments and the LaTeX command \\todo.
"""
import re

from analysis.annotation import Annotation


TODO_PATTERNS = [
    r'\\todo{(.*?)\}',
    r'%\s+todo:?\s(.+?)$'
]


class Todo(Annotation):
    def __init__(self, *args, word=None):
        annotation = f"There's something left you wanted to do."
        args = args + (annotation,)
        super().__init__(*args)
        self.word = word

    def markdown(self):
        lines = [
            f"line {self.linenumber} at character {self.index}: {self.word}"
        ]
        return "\n".join(lines)

    def __str__(self):
        lines = [
            f"line {self.linenumber} at character {self.index}: {self.annotation} - {self.word}",
            self.annotation_indicator()
        ]
        return "\n".join(lines)


def analyze(line, linenum, lang):
    annotations = []
    for pattern in TODO_PATTERNS:
        for match in re.finditer(pattern, line, flags=re.IGNORECASE):
            index = match.start()
            try:
                thing_todo = match.group(1)
            except AttributeError:
                continue
            annotation = Todo(linenum, line, index, word=thing_todo)
            annotations.append(annotation)

    return annotations


