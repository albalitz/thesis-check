"""Analysis of weasel word usage.

A weasel word is a word that makes a statement seem meaningful
but instead makes it vague enough for the speaker/writer to back out or deny having made a statement.
"""
import re

from analysis.annotation import Annotation
from analysis.utils import verify_match, matches_whole_words
from analysis.languages import Language

WEASEL_WORDS_EN = [
    r'(above|over|more than|below|under|up to) \d+',
    r'allegedly|supposedly|probably',
    r'(some )?(critics|experts|people) (say|state|(have )?found)',
    r'studies show',
    r'I (heard|read)',
    r'it (is|was|has been) (said|mentioned|noted)',
    r'multilingual',
    r'often',
    r'the (majority|minority)',
]

WEASEL_WORDS_DE = [
    r'(mehr|über|weniger|unter) als \d+',
    r'angeblich|vermutlich|mutmaßlich',
    r'Studien zeigen',
    r'Ich habe (gehört|gelesen)',
    r'es (wird|wurde) (gesagt|erwähnt)',
    r'multilinguistisch',
    r'oft|häufig',
    r'die (Mehrheit|Minderheit)',
    r'der Großteil'
]


class WeaselWord(Annotation):
    def __init__(self, *args, word=None):
        annotation = f"A weasel word makes a statement seem meaningful while allowing the speaker/writer to deny having made it. Provide supporting sources for your statements."
        args = args + (annotation,)
        super().__init__(*args)
        self.word = word

def analyze(line, linenum, lang):
    annotations = []

    if lang is Language.en_EN or lang is None:
        weasel_words = WEASEL_WORDS_EN
    elif lang is Language.de_DE:
        weasel_words = WEASEL_WORDS_DE

    for weasel_pattern in weasel_words:
        for match in re.finditer(weasel_pattern, line, flags=re.IGNORECASE):
            if (not verify_match(match, line)) or (not matches_whole_words(match, line)):
                continue

            index = match.start()
            annotation = WeaselWord(linenum, line, index, word=match.group(0))
            annotations.append(annotation)

    return annotations
