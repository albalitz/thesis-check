class Annotation:
    def __init__(self, linenumber, line, index, annotation, word=None):
        self.linenumber = linenumber
        self.line = line.strip()
        self.index = index
        self.annotation = annotation
        self.word = word

        # added later
        self.filename = None

    def annotation_indicator(self):
        """Returns two lines with an indicator pointing to the annotation.

        The first line is the annotation surrounding,
        the second line is an arrow pointing to the annotated character.

        Returns:
            str -- Two lines divided by a newline character.
        """
        try:
            width = len(self.word)
        except TypeError:
            width = 1
        indent = self.index
        indicator = f"{' ' * indent}{'^' * width}"
        return "\n".join([f"    '{self.line}'", f"     {indicator}"])

    def markdown(self):
        lines = [
            f"line {self.linenumber} at character {self.index}: {self.word}"
        ]
        return "\n".join(lines)

    def __str__(self):
        lines = [
            f"line {self.linenumber} at character {self.index}: {self.annotation}",
            self.annotation_indicator()
        ]
        return "\n".join(lines)
