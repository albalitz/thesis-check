from analysis import length
from analysis import word_complexity
from analysis import weasel_words
from analysis import todos
from analysis import contractions
from analysis import lexical_illusion


ANALYSES = [
    length,
    word_complexity,
    weasel_words,
    contractions,
    todos,
    lexical_illusion
]


def analyze_file(file_path, lang, run_analyses=None, no_run_analyses=None):
    """Analyzes a single file.

    Arguments:
        file_path {str} -- The path to the file.

    Returns:
        {list} -- A list of suboptimal formulations.
    """
    if run_analyses is None:
        run_analyses = []
    if no_run_analyses is None:
        no_run_analyses = []

    lines = []
    try:
        with open(file_path, "r") as f:
            lines = f.readlines()
    except FileNotFoundError as e:
        print(e)

    results = []
    for linenum, line in enumerate(lines):
        linenum += 1  # lines begin with 1
        for _analysis in ANALYSES:
            if _analysis in no_run_analyses or (run_analyses and _analysis not in run_analyses):
                continue

            line_results = _analysis.analyze(line.strip(), linenum, lang)
            for result in line_results:
                result.filename = file_path
                results.append(result)

    return results


def analyze_files(file_paths, lang, run_analyses=None, no_run_analyses=None):
    results = []
    for f in file_paths:
        file_results = analyze_file(f, lang, run_analyses=run_analyses, no_run_analyses=no_run_analyses)
        if file_results:
            results.append(file_results)
    return results


def analysis_help():
    """Prepare a help text about the available analyses.

    This takes the modules' names and docstrings
    and combines them into a help text.
    The modules' docstrings are indented to make it more readable.
    """
    analyses = [module.__name__ for module in ANALYSES]
    analysis_names = ', '.join(a.split('.')[-1] for a in analyses)
    available_analysis_names = f"Available analyses: {analysis_names}"

    output_lines = [
        available_analysis_names,
        ""
    ]
    for module in ANALYSES:
        output_lines.append(f"{module.__name__}")
        module_doc = module.__doc__
        module_doc = "\n".join(f"    {line}" for line in module_doc.split("\n"))
        output_lines.append(module_doc)

    return "\n".join(output_lines)
