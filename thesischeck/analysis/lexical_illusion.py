"""Analysis of lexical illusions.

Looks for duplicated words that are often overlooked by the human brain.
See http://www.thelinuxrain.com/articles/proofreading-for-illusions-with-grep-and-awk for an example.
"""
import re

from analysis.annotation import Annotation
from analysis.utils import verify_match, matches_whole_words

PATTERN = r'\b(\w+) \1\b|\b(\w+) \1\b'


class LexicalIllusion(Annotation):
    def __init__(self, *args, word=None):
        annotation = f"Lexical illusions are duplicated words the brain often overlooks."
        args = args + (annotation,)
        super().__init__(*args)
        self.word = word


def analyze(line, linenum, lang):
    annotations = []
    for match in re.finditer(PATTERN, line, flags=re.IGNORECASE):
        if not verify_match(match, line):
            continue

        index = match.start()
        annotation = LexicalIllusion(linenum, line, index, word=match.group(0))
        annotations.append(annotation)

    return annotations
