"""Analysis of sentence length.

Long sentences may make the text hard to read.
"""
from analysis.utils import sentences
from analysis.annotation import Annotation

MAX_WORD_COUNT = 34

class TooLongSentence(Annotation):
    def __init__(self, *args):
        annotation = f"Sentence too long. Try to keep it under {MAX_WORD_COUNT} words."
        args = args + (annotation,)
        super().__init__(*args)

    def markdown(self):
        lines = [
            f"line {self.linenumber} at character {self.index}",
        ]
        return "\n".join(lines)

    def __str__(self):
        lines = [
            f"line {self.linenumber}: {self.annotation}"
        ]
        return "\n".join(lines)


def analyze(line, linenum, lang):
    annotations = []
    sents = sentences(line)
    for sentence in sents:
        if len(sentence.split(" ")) > MAX_WORD_COUNT:
            words = sentence.split(" ")
            index = len(" ".join(words[:MAX_WORD_COUNT]))
            annotation = TooLongSentence(linenum, line, index)
            annotations.append(annotation)

    return annotations
