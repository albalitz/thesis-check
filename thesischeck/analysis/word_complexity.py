"""Analysis of word complexity.

Long, complex words may make the text hard to read.
"""
import pyphen

from analysis.annotation import Annotation
from analysis.languages import Language
from analysis.utils import cleanup_word
from analysis.utils import COMMENT_CHARS


MAX_SYLLABLE_COUNT_EN = 7
MAX_SYLLABLE_COUNT_DE = 12  # longer words are more common in German

dic = None

class ComplexWord(Annotation):
    def __init__(self, *args, word=None):
        annotation = f"This word seems complex. Maybe look for a shorter alternative."
        args = args + (annotation,)
        super().__init__(*args)
        self.word = word

    def __str__(self):
        lines = [
            f"line {self.linenumber} at character {self.index}: {self.annotation}",
            f"    {self.word}"
        ]
        return "\n".join(lines)


def syllables(word):
    """Calculate the syllables of a word.

    This uses pyphen to hyphenate the word
    and counts the produced hyphens.

    When encountering a word compound with a hyphen in it,
    the average syllable count of those combined words is returned.
    """
    if r"\url" in word:
        return 0

    if "-" in word:
        words = word.split("-")
        return sum(syllables(w) for w in words) // len(words)
    hyphenated = dic.inserted(word)
    return len(hyphenated.split("-"))

def analyze(line, linenum, lang):
    global dic
    dic = pyphen.Pyphen(lang = lang.name)

    if lang is Language.en_EN or lang is None:
        max_syllable_count = MAX_SYLLABLE_COUNT_EN
    elif lang is Language.de_DE:
        max_syllable_count = MAX_SYLLABLE_COUNT_DE

    annotations = []
    words = line.split(" ")
    for wordnum, word in enumerate(words):
        if word in COMMENT_CHARS:
            break

        word = cleanup_word(word)

        if syllables(word) > max_syllable_count:
            index = len(" ".join(words[:wordnum])) + 1
            annotation = ComplexWord(linenum, line, index, word=word)
            annotations.append(annotation)

    return annotations
