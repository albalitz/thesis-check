from enum import Enum

class Language(Enum):
    en_EN = "en_EN"
    de_DE = "de_DE"


DEFAULT_LANG = Language.en_EN


def get_language(lang):
    if lang is None:
        return DEFAULT_LANG

    try:
        return Language(lang)
    except ValueError:
        return NotImplemented


def language_help():
    """Prepare a help text about the available languages.
    """
    language_names = ', '.join(Language.__members__)
    available_analysis_names = f"Available languages: {language_names}"

    output_lines = [
        available_analysis_names,
        "",
        "   You can use one language at a time.",
        f"   If you don't specify one, {DEFAULT_LANG} will be used."
    ]

    return "\n".join(output_lines)
