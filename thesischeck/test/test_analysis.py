import os
import unittest

import analysis
from analysis.languages import Language
from analysis.length import analyze as analyze_length
from analysis.length import TooLongSentence
from analysis.word_complexity import analyze as analyze_word_complexity
from analysis.word_complexity import ComplexWord
from analysis.weasel_words import analyze as analyze_weasel_words
from analysis.weasel_words import WeaselWord
from analysis.todos import analyze as analyze_todos
from analysis.todos import Todo
from analysis.contractions import analyze as analyze_contractions
from analysis.contractions import Contraction
from analysis.lexical_illusion import analyze as analyze_lexical_illusion
from analysis.lexical_illusion import LexicalIllusion


testfile = os.path.join(os.path.dirname(__file__), "analysis_test_text.txt")

# this list should contain all of the possible annotation subclasses!
all_annotations = [
    TooLongSentence,
    ComplexWord,
    WeaselWord,
    Todo,
    Contraction,
    LexicalIllusion
]


class TestLengthAnalysis(unittest.TestCase):
    def test_sentence_length(self):
        line = "This is a text that contains a sentence that is supposed to be longer than the allowed limit of 34 words which is the limit according to Harvard professor D. H. Menzel who discovered that sentences exceeding this length are harder to follow than shorter sentences. Here's a shorter sentence that should not trigger anything."
        annotations = analyze_length(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 1)
        self.assertEqual(type(annotations[0]), TooLongSentence)


class TestWordComplexityAnalysis(unittest.TestCase):
    def test_word_complexity(self):
        line = r"This sentence contains a long and complex word: eierschalensollbruchstellenverursacher. Here's \emph{another} sentence with some\footnote{just a bit} LaTeX."
        annotations = analyze_word_complexity(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 1)
        self.assertEqual(type(annotations[0]), ComplexWord)

    def test_not_after_comment(self):
        line = "Here's a line with a complex word in a comment: % eierschalensollbruchstellenverursacher"
        annotations = analyze_word_complexity(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 0)

    def test_no_url(self):
        line = r"\gls{test}\footnote{\url{https://www.example.com/a_very_long_url_with_more_latex_around_it_that_is_not_supposed_to_be_a_complex_word/}}"
        annotations = analyze_word_complexity(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 0)


class TestWeaselWordsAnalysis(unittest.TestCase):
    def test_weasel_words(self):
        line = "Experts say this sentences contains weasel words - allegedly that's not cool."
        annotations = analyze_weasel_words(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 2)
        for a in annotations:
            self.assertEqual(type(a), WeaselWord)
        for weasel in ["Experts say", "allegedly"]:
            self.assertIn(weasel, [a.word for a in annotations])

    def test_not_after_comment(self):
        line = "Weasel can show up in comments: % allegedly that's okay"
        annotations = analyze_weasel_words(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 0)


class TestTodosAnalysis(unittest.TestCase):
    def test_todos(self):
        lines = [
            r"Here's something \todo{find this todo item}.",
            "This line also contains a  % todo: find this one too",
            "And another one: % todo do this"
        ]
        for lnum, line in enumerate(lines):
            lnum += 1
            annotations = analyze_todos(line, lnum, Language("en_EN"))
            self.assertEqual(len(annotations), 1)
            self.assertEqual(type(annotations[0]), Todo)

            if lnum == 1:
                self.assertEqual(annotations[0].word, "find this todo item")
            elif lnum == 2:
                self.assertEqual(annotations[0].word, "find this one too")
            elif lnum == 3:
                self.assertEqual(annotations[0].word, "do this")


class TestContractionsAnalysis(unittest.TestCase):
    def test_contractions(self):
        line = "Here is a line where I have used some contractions. They will be annotated soon."
        annotations = analyze_contractions(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 3)
        for a in annotations:
            self.assertEqual(type(a), Contraction)
        for c in ["Here is", "I have", "They will"]:
            self.assertIn(c, [a.word for a in annotations])
        for r in ["Here's", "I've", "They'll"]:
            self.assertIn(r, [a.contraction for a in annotations])

    def test_contractions_word_boundaries(self):
        line = "There's no conraction in 'Natürliche Sprache ist'."
        annotations = analyze_contractions(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 0)

    def test_not_after_comment(self):
        line = "A test % here is something that should not show up"
        annotations = analyze_contractions(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 0)


class TestLexicalIllusionAnalysis(unittest.TestCase):
    def test_lexical_illusions(self):
        line = "In this line there's a a lexical illusion that the the brain often overlooks."
        annotations = analyze_lexical_illusion(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 2)
        for a in annotations:
            self.assertEqual(type(a), LexicalIllusion)

    def test_lexical_illusions_whole_word(self):
        line = "Here is something that contains some letters at the end and the beginning of words."
        annotations = analyze_lexical_illusion(line, 1, Language("en_EN"))
        self.assertEqual(len(annotations), 0)


class TestThesisCheck(unittest.TestCase):
    def test_analysis(self):
        annotations = analysis.analyze_file(testfile, Language("en_EN"))  # no analysis explicitly in- or excluded
        self.assertGreater(len(annotations), 0)
        for annotation_type in all_annotations:
            self.assertIn(annotation_type, [type(a) for a in annotations])

    def test_analysis_specific(self):
        """
        Tests the --xyz arguments to run only specific checks.
        """
        annotations = analysis.analyze_file(testfile, Language("en_EN"), run_analyses=[analysis.contractions])
        for a in annotations:
            self.assertEqual(type(a), Contraction)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), run_analyses=[analysis.length])
        for a in annotations:
            self.assertEqual(type(a), TooLongSentence)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), run_analyses=[analysis.todos])
        for a in annotations:
            self.assertEqual(type(a), Todo)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), run_analyses=[analysis.weasel_words])
        for a in annotations:
            self.assertEqual(type(a), WeaselWord)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), run_analyses=[analysis.word_complexity])
        for a in annotations:
            self.assertEqual(type(a), ComplexWord)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), run_analyses=[analysis.lexical_illusion])
        for a in annotations:
            self.assertEqual(type(a), LexicalIllusion)

    def test_analysis_exclude(self):
        """
        Tests the --no-xyz arguments to exclude a specific check.
        """
        annotations = analysis.analyze_file(testfile, Language("en_EN"), no_run_analyses=[analysis.contractions])
        for a in annotations:
            self.assertNotEqual(type(a), Contraction)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), no_run_analyses=[analysis.length])
        for a in annotations:
            self.assertNotEqual(type(a), TooLongSentence)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), no_run_analyses=[analysis.todos])
        for a in annotations:
            self.assertNotEqual(type(a), Todo)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), no_run_analyses=[analysis.weasel_words])
        for a in annotations:
            self.assertNotEqual(type(a), WeaselWord)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), no_run_analyses=[analysis.word_complexity])
        for a in annotations:
            self.assertNotEqual(type(a), ComplexWord)

        annotations = analysis.analyze_file(testfile, Language("en_EN"), no_run_analyses=[analysis.lexical_illusion])
        for a in annotations:
            self.assertNotEqual(type(a), LexicalIllusion)
