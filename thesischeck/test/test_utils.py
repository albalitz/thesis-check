import unittest

from analysis.utils import cleanup_word

class TestUtils(unittest.TestCase):
    def test_cleanup_word(self):
        self.assertEqual(cleanup_word(r"\section{python}"), "python")
        self.assertEqual(cleanup_word(r"\begin{itemize}"), "")
        self.assertEqual(cleanup_word(r"word\footnote{wat}"), "word")
