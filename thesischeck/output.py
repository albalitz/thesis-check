import os

def output_as_text(annotation_lists):
    """Generate an output text.

    Arguments:
        annotation_lists {list} -- The results of analysis.analyze_files

    Returns:
        str -- A string containing the analysis results in a readable form
    """
    if all(len(l) == 0 for l in annotation_lists):
        return ""

    output_lines = [
        f"Found {sum(len(l) for l in annotation_lists)} issues in {len(annotation_lists)} files."
    ]

    for file_annotations in annotation_lists:
        filename = file_annotations[0].filename
        output_lines.append("\n")
        output_lines.append(filename)
        for annotation in file_annotations:
            output_lines.append(f"{annotation}")
            output_lines.append("")

    return "\n".join(output_lines)

def output_as_markdown(annotation_lists):
    """Generate an output text.

    Formats the output text in valid markdown to allow usage for a GitHub/GitLab issue.

    Arguments:
        annotation_lists {list} -- The results of analysis.analyze_files

    Returns:
        str -- A string containing the analysis results in valid markdown
    """
    if all(len(l) == 0 for l in annotation_lists):
        return ""

    output_lines = [
        "# Analysis Results",
        f"Found {sum(len(l) for l in annotation_lists)} issues in {len(annotation_lists)} files.",
        "\n"
    ]

    for file_annotations in annotation_lists:
        filename = file_annotations[0].filename
        filename = os.path.basename(filename)
        output_lines.append(f"## {filename}")

        # group by annotation type
        annotation_types = {}
        for annotation in file_annotations:
            annotation_types[type(annotation)] = annotation_types.get(type(annotation), []) + [annotation]

        for annotation_type, annotations in annotation_types.items():
            output_lines.append(f"### {annotation_type.__name__}")
            output_lines.append(f"{annotations[0].annotation}\n")

            for annotation in annotations:
                output_lines.append(f"- [ ] {annotation.markdown()}")
            output_lines.append("\n")

    return "\n".join(output_lines)
