#!/usr/bin/env python3
import argparse

import analysis
import output
from analysis import analyze_files
from analysis import analysis_help
from analysis.languages import language_help
from analysis.languages import get_language


def add_analysis_arguments(argument_parser):
    """Dynamically adds arguments for available analyses.

    For every available analysis this generates a command line argument
    and adds it to the argument parser.
    """
    for analysis_module in analysis.ANALYSES:
        analysis_name = analysis_module.__name__.split(".")[-1]
        argument_parser.add_argument(
            f"--{analysis_name}",
            action = "store_true",
            help = f"Include the '{analysis_name}' analysis in run analyses (all if none explicitly given)"
        )

        argument_parser.add_argument(
            f"--no-{analysis_name}",
            action = "store_true",
            help = f"Exclude the '{analysis_name}' analysis from the analyses"
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Analyze your thesis for common writing mistakes.")

    parser.add_argument(
        "--analysis-help",
        action = "store_true",
        help = "Show available analyses and their descriptions and exit"
    )
    parser.add_argument(
        "--markdown",
        action = "store_true",
        help = "Format output as markdown to allow using it for a GitHub/GitLab issue"
    )
    parser.add_argument(
        "-l", "--language",
        action = "store",
        help = "Set the analysis language"
    )
    parser.add_argument(
        "--language-help",
        action = "store_true",
        help = "Show available languages and exit"
    )

    add_analysis_arguments(parser)

    parser.add_argument(
        "files",
        metavar = "file",
        nargs = "*",
        help = "a file to be checked"
    )

    args = parser.parse_args()

    if args.analysis_help:
        print(analysis_help())
        quit()

    if args.language_help:
        print(language_help())
        quit()

    explicit_analyses = []
    excluded_analyses = []
    for analysis_module in analysis.ANALYSES:
        analysis_name = analysis_module.__name__.split(".")[-1]
        if args.__dict__[analysis_name]:
            explicit_analyses.append(analysis_module)
        if args.__dict__[f"no_{analysis_name}"]:
            excluded_analyses.append(analysis_module)

    if any(a in excluded_analyses for a in explicit_analyses):
        print("error: an analysis can't be specified to be run and not run at the same time.")
        quit()

    if not args.files:
        print("error: argument missing: file")
        quit()
    annotation_lists = analyze_files(args.files, run_analyses=explicit_analyses, no_run_analyses=excluded_analyses, lang=get_language(args.language))

    if args.markdown:
        output_text = output.output_as_markdown(annotation_lists)

    else:
        output_text = output.output_as_text(annotation_lists)

    print(output_text)
