# thesis-check
Analyze your thesis for common writing mistakes.

Proofreading is hard and a time-consuming part of writing.
I wrote this tool while working on my bachelor thesis to make it a bit less time-consuming.

## resources
These documents build the basis of this implementation.

- [Avoid these technical writing mistakes](http://www.d.umn.edu/~dlong/writetips.pdf), Robert W. Bly.
- [Words To Avoid in Educational Writing](https://css-tricks.com/words-avoid-educational-writing/), Chris Coyier
- [3 shell scripts to improve your writing, or "My Ph.D. advisor rewrote himself in bash."](http://matt.might.net/articles/shell-scripts-for-passive-voice-weasel-words-duplicates/), Matt Might

### related projects
I also found these projects doing similar things
and based thesis-check on their idea and checks for some of the same mistaktes.

- [btford/write-good](https://github.com/btford/write-good)

## analyses
This is an overview of the available analyses.
You can display this overview by using the `--analysis-help` argument.

```shell
$ thesischeck/thesischeck.py --analysis-help
Available analyses: length, word_complexity, weasel_words, contractions, todos, lexical_illusion

analysis.length
    Analysis of sentence length.
    
    Long sentences may make the text hard to read.
    
analysis.word_complexity
    Analysis of word complexity.
    
    Long, complex words may make the text hard to read.
    
analysis.weasel_words
    Analysis of weasel word usage.
    
    A weasel word is a word that makes a statement seem meaningful
    but instead makes it vague enough for the speaker/writer to back out or deny having made a statement.
    
analysis.contractions
    Analysis of contractions.
    
    Looks for uses of non-contracted words like 'is not' instead of isn't.
    See https://en.wikipedia.org/wiki/Contraction_(grammar)
    
analysis.todos
    Analysis of todos.
    
    Looks for leftover or overlooked todo comments and the LaTeX command \todo.
    
analysis.lexical_illusion
    Analysis of lexical illusions.
    
    Looks for duplicated words that are often overlooked by the human brain.
    See http://www.thelinuxrain.com/articles/proofreading-for-illusions-with-grep-and-awk for an example.
```

## how to use
```shell
$ thesischeck/thesischeck.py -h
usage: thesischeck.py [-h] [--analysis-help] [--markdown] [-l LANGUAGE]
                      [--language-help] [--length] [--no-length]
                      [--word_complexity] [--no-word_complexity]
                      [--weasel_words] [--no-weasel_words] [--contractions]
                      [--no-contractions] [--todos] [--no-todos]
                      [--lexical_illusion] [--no-lexical_illusion]
                      [file [file ...]]

Analyze your thesis for common writing mistakes.

positional arguments:
  file                  a file to be checked

optional arguments:
  -h, --help            show this help message and exit
  --analysis-help       Show available analyses and their descriptions and
                        exit
  --markdown            Format output as markdown to allow using it for a
                        GitHub/GitLab issue
  -l LANGUAGE, --language LANGUAGE
                        Set the analysis language
  --language-help       Show available languages and exit
  --length              Include the 'length' analysis in run analyses (all if
                        none explicitly given)
  --no-length           Exclude the 'length' analysis from the analyses
  --word_complexity     Include the 'word_complexity' analysis in run analyses
                        (all if none explicitly given)
  --no-word_complexity  Exclude the 'word_complexity' analysis from the
                        analyses
  --weasel_words        Include the 'weasel_words' analysis in run analyses
                        (all if none explicitly given)
  --no-weasel_words     Exclude the 'weasel_words' analysis from the analyses
  --contractions        Include the 'contractions' analysis in run analyses
                        (all if none explicitly given)
  --no-contractions     Exclude the 'contractions' analysis from the analyses
  --todos               Include the 'todos' analysis in run analyses (all if
                        none explicitly given)
  --no-todos            Exclude the 'todos' analysis from the analyses
  --lexical_illusion    Include the 'lexical_illusion' analysis in run
                        analyses (all if none explicitly given)
  --no-lexical_illusion
                        Exclude the 'lexical_illusion' analysis from the
                        analyses
```

When your thesis consists of multiple files, you can check all of them at once by using your shell's wildcard expansion:
`thesischeck/thesischeck.py /path/to/your/thesis/chapters/*.tex`

Currently thesis-check supports the following languages:
- English
- German

### running tests
Run all the tests by running
```shell
$ python -m unittest discover -v -s thesischeck
```
in the root directory of this repository.
